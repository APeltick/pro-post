<?php

/*
Plugin Name: pro-post-widget
Plugin URI: http://test.com
Description: Pro Post Widget
Version: 0.0.1
Author: Alex
Author URI: http://test.com
*/

class Pro_Post_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct( 'pro_post_widget', 'Pro Post',
			array( 'description' => 'Pro Post Widget' ) );

		add_action( 'wp_ajax_post_search', array( $this, 'post_search' ));
		add_action( 'wp_ajax_nopriv_post_search', array( $this, 'post_search' ));
		add_action( 'wp_enqueue_scripts',  array( $this, 'add_pro_post_js' ));
	}

	public function widget( $args, $instance ) {

		global $post;
		$posts_list = get_posts( array(
			'posts_per_page' => $instance['number'],
		) );

		?>
		<form>
			<h3><?php _e( 'Posts search' ) ?></h3>
			<p>
				<input type="text" id="post-title" placeholder="<?php _e( 'Find post by title' ) ?>">
			</p>
			<p>
				<label for="post-date"><?php _e( 'Posts date' ) ?></label>
				<input type="date" id="post-date">
			</p>
            <input type="hidden" id="post-number" name="post-number" value="<?php $instance['number'] ?>">
		</form>
		<div>
			<ul id="post-search">
				<?php
				foreach( $posts_list as $post ){
					setup_postdata($post);
					?>
                        <li>
                            <a href="<?php the_permalink() ?>"><?php _e( the_title() ) ?></a>
                        </li>
					<?php
				}
				wp_reset_postdata();
				?>
			</ul>
		</div>
		<?php
	}


	public function form( $instance ) {
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 2;
		?>
		<div>
			<p>
				<label
					for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of post to show:' ); ?></label>
				<input id="<?php echo $this->get_field_id( 'number' ); ?>"
				       name="<?php echo $this->get_field_name( 'number' ); ?>"
				       type="number" step="1" min="1"
				       value="<?php echo $number; ?>" size="3"/>
			</p>
		</div>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['number'] = (int) $new_instance['number'];
		return $instance;
	}

	public function post_search() {

		$args = array(
		        'posts_per_page' => $_POST['post_number'],
		        'order'          => 'DESC'
        );

		if ( ! empty( $_POST['post_date'] ) && ! empty( $_POST['post_title'] ) ) {
			$args['date_query'] = array(
				array(
					'after' => $_POST['post_date'],
				)
			);
			$args['s'] = $_POST['post_title'];
		} elseif ( ! empty( $_POST['post_date'] ) ) {
			$args['date_query'] = array(
				array(
					'after' => $_POST['post_date'],
				)
			);
		} elseif ( ! empty( $_POST['post_title'] ) ) {
			$args['s'] = $_POST['post_title'];
		}

		global $post;
		$postslist = get_posts( $args );
		ob_start();
		foreach ( $postslist as $post ) {
			setup_postdata( $post );
			?>
            <li>
                <a href="<?php the_permalink() ?>"><?php _e( the_title() ) ?></a>
            </li>
            <?php
		}
		$result = ob_get_contents();
		ob_end_clean();
		wp_reset_postdata();

		echo $result;
		wp_die();
	}

	public function add_pro_post_js() {
		wp_enqueue_script( 'pro_post', plugins_url( 'pro-post-ajax.js', __FILE__ ), array('jquery') );
		wp_localize_script( 'pro_post', 'postSearch',
			array(
				'ajaxurl' => admin_url('admin-ajax.php')
			) );
	}

}

add_action( 'widgets_init', function () {
	register_widget( 'Pro_Post_Widget' );
} );
