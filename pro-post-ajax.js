;jQuery(document).ready(function($) {

    var number = $("#post-number").val();

    $('#post-date').change(function(){

        var date = $("#post-date").val(),
            title = $("#post-title").val(),
            data = {
                action: 'post_search',
                post_title: title,
                post_date: date,
                post_number: number
            };

        jQuery.post( postSearch.ajaxurl, data, function(response) {
            $('#post-search').html(response);
        });

    });

    $('#post-title').keyup(function(){

        var date = $("#post-date").val(),
            title = $("#post-title").val(),
            data = {
                action: 'post_search',
                post_title: title,
                post_date: date,
                post_number: number
            };

        jQuery.post( postSearch.ajaxurl, data, function(response) {
            $('#post-search').html(response);
        });

    });

});

